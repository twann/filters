# Title: Twann's Anti-Conspiracist List
# Version: 220118
# Description: A blocklist aiming to block websites that promote and spread conspiracy theories, misinformation and far-right ideologies.
# Homepage: https://codeberg.org/twann/filters
# License: https://codeberg.org/twann/filters/src/branch/master/LICENSE
# Issues: https://codeberg.org/twann/filters/issues

# Conspiracy theories, transphobia, antivax
0.0.0.0		humansarefree.com
0.0.0.0		humansbefree.com

# Conspiracy theories, antivax
0.0.0.0		luis46pr.wordpress.com

# Conspiracy theories, antivax
0.0.0.0		eternian.wordpress.com

# Conspiracy theories, racism, transphobia, antivax
0.0.0.0		summit.news

# Conspiracy theories, transphobia, homophobia, antivax
0.0.0.0		www.brighteon.com
0.0.0.0		brighteon.com
0.0.0.0		brighteon.social
0.0.0.0		brighteon.tv

# Conspiracy theories
0.0.0.0		allnewspipeline.com

# Conspiracy theories
0.0.0.0		patriots.win

# Conspiracy theories, antivax
0.0.0.0		www.whatfinger.com
0.0.0.0		whatfinger.com
0.0.0.0		generaldispatch.whatfinger.com
0.0.0.0		webmaster.whatfinger.com
0.0.0.0		news.whatfinger.com
0.0.0.0		videos.whatfinger.com
0.0.0.0		entertainment.whatfinger.com
0.0.0.0		mainstream.whatfinger.com
0.0.0.0		linkshare.whatfinger.com
0.0.0.0		deos.whatfinger.com
0.0.0.0		choiceclips.whatfinger.com

# Conspiracy theories, antivax
0.0.0.0		www.abovetopsecret.com
0.0.0.0		abovetopsecret.com
0.0.0.0		theworldwatch.com
0.0.0.0		pressbull.com
0.0.0.0		ww38.breaking-cnn.com
0.0.0.0		breaking-cnn.com
0.0.0.0		cnn-trending.com
0.0.0.0		countynewsroom.info
0.0.0.0		www6.countynewsroom.info
0.0.0.0		drudgereport.com.co
0.0.0.0		ww25.drudgereport.com.co
0.0.0.0		fox-news24.com
0.0.0.0		ww1.fox-news24.com
0.0.0.0		globalresearch.ca
0.0.0.0		www.globalresearch.ca
0.0.0.0		globalresearch.org
0.0.0.0		www.globalresearch.org
0.0.0.0		huzlers.com
0.0.0.0		www.huzlers.com
0.0.0.0		viralcocaine.com
0.0.0.0		legrandreveil.co
0.0.0.0		www.redvoicemedia.com
0.0.0.0		redvoicemedia.com
0.0.0.0		www.theburningplatform.com
0.0.0.0		theburningplatform.com

