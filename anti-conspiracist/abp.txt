[Adblock Plus 3.1]
! Title: Twann's Anti-Conspiracist List
! Version: 220118
! Description: A blocklist aiming to block websites that promote and spread conspiracy theories, misinformation and far-right ideologies.
! Homepage: https://codeberg.org/twann/filters
! License: https://codeberg.org/twann/filters/src/branch/master/LICENSE
! Issues: https://codeberg.org/twann/filters/issues

! Conspiracy theories, transphobia, antivax
||humansarefree.com^
||humansbefree.com^

! Conspiracy theories, antivax
||luis46pr.wordpress.com^

! Conspiracy theories, antivax
||eternian.wordpress.com^

! Conspiracy theories, racism, transphobia, antivax
||summit.news^

! Conspiracy theories, transphobia, homophobia, antivax
||www.brighteon.com^
||brighteon.com^
||brighteon.social^
||brighteon.tv^

! Conspiracy theories
||allnewspipeline.com^

! Conspiracy theories
||patriots.win^

! Conspiracy theories, antivax
||www.whatfinger.com^
||whatfinger.com^
||generaldispatch.whatfinger.com^
||webmaster.whatfinger.com^
||news.whatfinger.com^
||videos.whatfinger.com^
||entertainment.whatfinger.com^
||mainstream.whatfinger.com^
||linkshare.whatfinger.com^
||deos.whatfinger.com^
||choiceclips.whatfinger.com^

! Conspiracy theories, antivax
||www.abovetopsecret.com^
||abovetopsecret.com^
||theworldwatch.com^
||pressbull.com^
||ww38.breaking-cnn.com^
||breaking-cnn.com^
||cnn-trending.com^
||countynewsroom.info^
||www6.countynewsroom.info^
||drudgereport.com.co^
||ww25.drudgereport.com.co^
||fox-news24.com^
||ww1.fox-news24.com^
||globalresearch.ca^
||www.globalresearch.ca^
||globalresearch.org^
||www.globalresearch.org^
||huzlers.com^
||www.huzlers.com^
||viralcocaine.com^
||legrandreveil.co^
||www.redvoicemedia.com^
||redvoicemedia.com^
||www.theburningplatform.com^
||theburningplatform.com^

