# Filters

Twann's Filters are blocklists compatible with a lot of different ad-blockers. They focus on privacy and freedom online, and are available in different formats.

## Overview

| Title | Blocks | Source | Mirror | Tor mirror |
| --- | --- | --- | --- | --- |
| Twann's List | ads + trackers + annoyances + malwares + social icons | [hosts](https://codeberg.org/twann/filters/raw/branch/master/twann-list/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://codeberg.org/twann/filters/raw/branch/master/twann-list/hosts/0.0.0.0.txt) / [adblock plus](https://codeberg.org/twann/filters/raw/branch/master/twann-list/abp.txt) / [domain list](https://codeberg.org/twann/filters/raw/branch/master/twann-list/domains.txt) / [tblock](https://codeberg.org/twann/filters/raw/branch/master/twann-list/tblock.tbf) | [hosts](https://0xacab.org/twann/filters/-/raw/master/twann-list/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://0xacab.org/twann/filters/-/raw/master/twann-list/hosts/0.0.0.0.txt) / [adblock plus](https://0xacab.org/twann/filters/-/raw/master/twann-list/abp.txt) / [domain list](https://0xacab.org/twann/filters/-/raw/master/twann-list/domains.txt) / [tblock](https://0xacab.org/twann/filters/-/raw/master/twann-list/tblock.tbf) | [hosts](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/twann-list/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/twann-list/hosts/0.0.0.0.txt) / [adblock plus](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/twann-list/abp.txt) / [domain list](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/twann-list/domains.txt) / [tblock](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/twann-list/tblock.tbf) |
| Twann's Anti IP-Loggers Blocklist | ip-loggers | [hosts](https://codeberg.org/twann/filters/raw/branch/master/anti-ip-loggers/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://codeberg.org/twann/filters/raw/branch/master/anti-ip-loggers/hosts/0.0.0.0.txt) / [adblock plus](https://codeberg.org/twann/filters/raw/branch/master/anti-ip-loggers/abp.txt) / [domain list](https://codeberg.org/twann/filters/raw/branch/master/anti-ip-loggers/domains.txt) / [tblock](https://codeberg.org/twann/filters/raw/branch/master/anti-ip-loggers/tblock.tbf) | [hosts](https://0xacab.org/twann/filters/-/raw/master/anti-ip-loggers/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://0xacab.org/twann/filters/-/raw/master/anti-ip-loggers/hosts/0.0.0.0.txt) / [adblock plus](https://0xacab.org/twann/filters/-/raw/master/anti-ip-loggers/abp.txt) / [domain list](https://0xacab.org/twann/filters/-/raw/master/anti-ip-loggers/domains.txt) / [tblock](https://0xacab.org/twann/filters/-/raw/master/anti-ip-loggers/tblock.tbf) | [hosts](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-ip-loggers/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-ip-loggers/hosts/0.0.0.0.txt) / [adblock plus](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-ip-loggers/abp.txt) / [domain list](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-ip-loggers/domains.txt) / [tblock](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-ip-loggers/tblock.tbf) |
| Twann's Anti-Conspiracist List | fake news + conspiracy theories | [hosts](https://codeberg.org/twann/filters/raw/branch/master/anti-conspiracist/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://codeberg.org/twann/filters/raw/branch/master/anti-conspiracist/hosts/0.0.0.0.txt) / [adblock plus](https://codeberg.org/twann/filters/raw/branch/master/anti-conspiracist/abp.txt) / [domain list](https://codeberg.org/twann/filters/raw/branch/master/anti-conspiracist/domains.txt) / [tblock](https://codeberg.org/twann/filters/raw/branch/master/anti-conspiracist/tblock.tbf) | [hosts](https://0xacab.org/twann/filters/-/raw/master/anti-conspiracist/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](https://0xacab.org/twann/filters/-/raw/master/anti-conspiracist/hosts/0.0.0.0.txt) / [adblock plus](https://0xacab.org/twann/filters/-/raw/master/anti-conspiracist/abp.txt) / [domain list](https://0xacab.org/twann/filters/-/raw/master/anti-conspiracist/domains.txt) / [tblock](https://0xacab.org/twann/filters/-/raw/master/anti-conspiracist/tblock.tbf) | [hosts](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-conspiracist/hosts/127.0.0.1.txt) / [hosts (0.0.0.0)](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-conspiracist/hosts/0.0.0.0.txt) / [adblock plus](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-conspiracist/abp.txt) / [domain list](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-conspiracist/domains.txt) / [tblock](http://wmj5kiic7b6kjplpbvwadnht2nh2qnkbnqtcv3dyvpqtz7ssbssftxid.onion/twann/filters/-/raw/master/anti-conspiracist/tblock.tbf) |

## How to use?

Since the blocklists are available in different filter lists formats, you can use a large variety of software.

Here are some personal recommendations:

- [AdAway](https://adaway.org/)
- [uBlock Origin](https://github.com/gorhill/uBlock)
- [TBlock](https://tblock.codeberg.page/)
- [pi-hole](https://pi-hole.net/)

## Subscribe with [TBlock](https://tblock.codeberg.page/):

**Twann's List**:

```sh
tblock -S twann-list
```

**Twann's Anti IP-Loggers Blocklist**:

```sh
tblock -S twann-anti-ip-loggers
```

**Twann's Anti-Conspiracist List**

```sh
tblock -S twann-anti-conspiracist
```

## Found a false positive? Want to request a domain to be blocked?

[Open an issue](https://codeberg.org/twann/filters/issues) or send an email to: `tw4nn at riseup dot net` [[PGP]](https://keys.openpgp.org/pks/lookup?op=get&options=mr&search=0xa57da10c6a622811b52d609ba964862b572e3094).

## License
Twann's Filters are licensed under [CC0](https://codeberg.org/twann/filters/src/branch/master/LICENSE), which means that they are released into the public domain.
