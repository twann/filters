# This script needs the package "tblock" to be installed.
# More information: https://tblock.codeberg.page/install

for x in **/abp.txt; do 
	tblockc -C "$x" -qs list -o $(dirname "$x")/domains.txt
	tblockc -C "$x" -qcs hosts -o $(dirname "$x")/hosts/0.0.0.0.txt -0
	tblockc -C "$x" -qcs hosts -o $(dirname "$x")/hosts/127.0.0.1.txt
	tblockc -C "$x" -qcs tblock -o $(dirname "$x")/tblock.txt
done
